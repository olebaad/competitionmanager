# CompetitionManager
This is a console application utilizing a code first workflow to create the initial migrations for a single class (competitor) and the corresponding database using entity framework core. The application allows a user to manage information for participants in a boxing tournament.

## Database
The database the application utilizes has the following tables:

- **Participants**
Holds the information about the boxers competing in the tournament.

- **Coaches**
Holds the information about the coaches of the participants competing in the tournament.

- **Teams**
Holds the information about the teams of the participants competing in the tournament.

- **Skills**
Holds the information about the skills of the participants competing in the tournament.

- **ParticipantSkills**
Joining class for the many to many relationship between Participants and Skills.

## Application components
### Participant class
The Participant class has the following properties:

- **Id**
- **HeightInCm**
- **WeightInGrams**
- **FirstName**
- **LastName**
- **Nickname**
- **Division**
The name of the weight-class the boxer competes in.
- **Team**
This is a one to many relationship to Team the class. (One team can have several Participants)
- **Coach**
This is a one to one relationship to the class called Coach.
- **ParticipantSkill**
This is a many to many relationship to the Skill class through a joining table called ParticipantSkills.

### Teams class
- **Id**
- **Name**
- **NumberOfMembers**
- **Location**   
- **Participants**
This is a many to one relationship to the Participant class.

### Coach class
- **Id**
- **FirstName**
- **LastName**
- **Location**   
- **Participant**
This is a one to one relationship to the Participant class.

### Skill class
- **Id**
- **Type**
- **Level**
This is an integer representing the level the Participant is at for the particular skill-type.
- **ParticipantSkills**
This is a many to many relationship to the Skill class through a joining class called ParticipantSkills.

### ParticipantSkill class
Joining class for the many to many relationship between Participants and Skills.
- **ParticipantId**
- **SkillId**

### Program class
Runs a customized example managing a particular boxing tournament.

## Author
Ole Baadshaug