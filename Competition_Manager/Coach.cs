﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Competition_Manager
{
    public class Coach
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int? ParticipantId { get; set; }
        public Participant Participant { get; set; }

        public Coach()
        {

        }
    }
}
