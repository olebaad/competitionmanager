﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Competition_Manager
{
    public class CompetitionDbContext : DbContext
    {
        public DbSet<Participant> Participants { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<Coach> Coaches { get; set; }
        public DbSet<Skill> Skills { get; set; }
        public DbSet<ParticipantSkill> ParticipantSkills { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseSqlServer(@"Server=PC7262\SQLEXPRESS01;Database=ParticipantsBoxing;Trusted_Connection=True;MultipleActiveResultSets=true");
            optionsBuilder.UseSqlServer(@"Server=PC7262\SQLEXPRESS01;Database=BoxingTournament;Trusted_Connection=True;MultipleActiveResultSets=true");
        }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ParticipantSkill>().HasKey(ps => new { ps.ParticipantId, ps.SkillId });
        }
    }
}
