﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Competition_Manager
{
    public class ParticipantSkill
    {
        public int ParticipantId { get; set; }
        public Participant Participant { get; set; }
        public int SkillId { get; set; }
        public Skill Skill { get; set; }

        public ParticipantSkill()
        {

        }
    }
}
