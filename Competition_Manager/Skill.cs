﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Competition_Manager
{
    public class Skill
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public int Level { get; set; }
        public ICollection<ParticipantSkill> ParticipantSkills { get; set; }
        public Skill()
        {

        }
    }
}
