﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Competition_Manager
{
    public class Team
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int NumberOfMembers { get; set; }
        public string Location { get; set; }
        public ICollection<Participant> Participants { get; set; }

        public Team()
        {

        }
    }
}
