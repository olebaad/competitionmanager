﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Competition_Manager
{
    public class Participant
    {
        public int Id { get; set; }
        public int HeightInCm { get; set; }
        public int WeightInGrams { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string NickName { get; set; }
        public string Division { get; set; }
        public Team Team { get; set; }
        public int? TeamId { get; set; }
        public Coach Coach { get; set; }
        public ICollection<ParticipantSkill> ParticipantSkills { get; set; }

        public Participant()
        {

        }
    }
}
