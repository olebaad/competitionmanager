﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Competition_Manager
{
    class Program
    {
        static void Main(string[] args)
        {
            ClearDatabase();
            List<Team> teams = new List<Team>()
            {
                CreateTeam("Kronk Gym", 150, "Detroit, MI"),
                CreateTeam("Skyy Boxing Gym", 50, "Northport, AL"),
                CreateTeam("Julian Magdaleno Gym", 120, "Gudalajara, Mexico"),
                CreateTeam("The Money Team", 1, "Las Vegas, NV"),
                CreateTeam("Wild Card Boxing Club", 250, "Hollywood, CA"),
            };
            List<Skill> skills = new List<Skill>()
            {
                CreateSkill("Speed", 50),
                CreateSkill("Speed", 100),
                CreateSkill("Power", 50),
                CreateSkill("Power", 100),
                CreateSkill("IQ", 10),
                CreateSkill("IQ", 50),
                CreateSkill("IQ", 100),
            };
            List<Coach> coaches = new List<Coach>()
            {
                CreateCoach("Sugar", "Hill"),
                CreateCoach("Jay", "Deas"),
                CreateCoach("Eddy", "Reynoso"),
                CreateCoach("Roger", "Mayweather"),
                CreateCoach("Freddie", "Roach"),
                CreateCoach("Jonathan", "Banks"),
            };
            List<Participant> participants = new List<Participant>()
            {
                CreateBoxer(206, 123.83, "Tyson","Fury", "\"The Gypsy King\""),
                CreateBoxer(201, 106.59, "Deontay","Wilder", "\"The Bronze Bomber\""),
                CreateBoxer(175, 75.3, "Saúl", "Álvares", "\"Canelo\""),
                CreateBoxer(173, 65.9, "Floyd", "Mayweather", "\"Money\""),
                CreateBoxer(166, 65.8, "Manny", "Pacquiao","\"Pacman\""),
                CreateBoxer(179, 74.8, "Gennady", "Golovkin", "\"GGG\""),
            };

            //Array of customized skill set indecies (Just for seeding purposes)
            List<int[]> skillsInRelationToBoxer = new List<int[]>
            {
                new int[] { 1, 2, 6},
                new int[] { 0, 3, 4},
                new int[] { 0, 3, 5},
                new int[] { 1, 3, 6},
                new int[] { 1, 3, 5},
                new int[] { 0, 3, 5},

            };

            //Array to keep track of team in reation to boxer (Just for seeding purposes)
            string[] teamInRelationToBoxer = { 
                "Kronk Gym", 
                "Skyy Boxing Gym", 
                "Julian Magdaleno Gym", 
                "The Money Team", 
                "Wild Card Boxing Club", 
                "Kronk Gym" 
            };

            SeedTeams(teams);
            SeedSkills(skills);
            SeedBoxers(participants, teamInRelationToBoxer, skillsInRelationToBoxer, skills);
            SeedCoaches(coaches, participants);
            
            GetTournamentInfo();
        }
        
        public static void GetTournamentInfo()
        {
            Console.WriteLine("Welcome to the Super 6 Tournament!!\n");
            Console.WriteLine("The figthers competing are:\n");
            using (CompetitionDbContext boxerContext = new CompetitionDbContext())
            {
                List<Participant> allBoxers = boxerContext.Participants.ToList();
                int counter = 1;
                foreach (Participant boxer in allBoxers)
                {
                    Coach coach = boxerContext.Coaches.Where(s => s.ParticipantId == boxer.Id).Single();
                    Team team = boxerContext.Teams.Find(boxer.TeamId);
                    List<ParticipantSkill> participantSkills = boxerContext.ParticipantSkills.Where(s => s.ParticipantId == boxer.Id).ToList();

                    double weightInKg = boxer.WeightInGrams / 1000.0;

                    Console.WriteLine($"Figther {counter}:" +
                        $"\nName:\t\t{boxer.FirstName} {boxer.LastName}\n" +
                        $"Division:\t{boxer.Division}\n" +
                        $"Weight:\t\t{weightInKg}kg\n" +
                        $"Height:\t\t{boxer.HeightInCm}cm\n" +
                        $"Team:\t\t{team.Name}\n" +
                        $"Coach:\t\t{coach.FirstName} {coach.LastName}"
                    );
                    foreach(ParticipantSkill participantSkill in participantSkills)
                    {
                        Skill skill = boxerContext.Skills.Where(s => s.Id == participantSkill.SkillId).Single();
                        Console.WriteLine($"{skill.Type}-Level:\t{skill.Level}");
                    }
                    Console.WriteLine();
                    counter++;
                }
            }
        }
        public static void SeedCoaches(List<Coach> coaches, List<Participant> boxers)
        {
            using (CompetitionDbContext coachContext = new CompetitionDbContext())
            {
                int counter = 0;
                foreach (Coach coach in coaches)
                {
                    Participant boxerAssignedCoach = coachContext.Participants.Where(s => s.FirstName == boxers[counter].FirstName).Single();
                    coach.Participant = boxerAssignedCoach;

                    coachContext.Coaches.Add(coach);
                    counter++;
                }
                coachContext.SaveChanges();
            }
        }
        public static void SeedSkills(List<Skill> skills)
        {
            using (CompetitionDbContext skillContext = new CompetitionDbContext())
            {
                foreach (Skill skill in skills)
                {
                    skillContext.Skills.Add(skill);
                }

                skillContext.SaveChanges();
            }
        }
        public static void SeedTeams(List<Team> teams)
        {
            using (CompetitionDbContext teamContext = new CompetitionDbContext())
            {
                foreach (Team team in teams)
                {
                    teamContext.Teams.Add(team);
                }

                teamContext.SaveChanges();
            }
        }
        public static void SeedBoxers(List<Participant> boxers, string[] teams, List<int[]> skillIndex, List<Skill> skills)
        {
            using(CompetitionDbContext boxerContext = new CompetitionDbContext())
            {
                int counter = 0;
                foreach(Participant boxer in boxers)
                {
                    //Add a customized skillset
                    List<ParticipantSkill> participantSkill = new List<ParticipantSkill>();
                    foreach (int index in skillIndex[counter])
                    {
                        participantSkill.Add(
                            new ParticipantSkill
                            {
                                Skill = boxerContext.Skills.Where(s => (s.Type == skills[index].Type && s.Level == skills[index].Level)).Single(),
                                Participant = boxer,
                            }
                        );
                    }

                    boxer.ParticipantSkills = participantSkill;

                    //Assign a team
                    Team teamAssignedBoxer = boxerContext.Teams.Where(s => s.Name == teams[counter]).Single();
                    boxer.Team = teamAssignedBoxer;

                    boxerContext.Participants.Add(boxer);
                    counter++;
                }

                boxerContext.SaveChanges();
            }
        }
        public static Skill CreateSkill(string type, int level)
        {
            Skill skill = new Skill();
            skill.Type = type;
            skill.Level = level;

            return skill;
        }
        public static Team CreateTeam(string name, int members, string location)
        {
            Team team = new Team();
            team.Name = name;
            team.NumberOfMembers = members;
            team.Location = location;

            return team;
        }
        public static Coach CreateCoach(string firstName, string lastName, int id = -1)
        {
            Coach coach = new Coach();
            coach.FirstName = firstName;
            coach.LastName = lastName;
            if(id != -1)
            {
                coach.ParticipantId = id;
            }
            return coach;
        }
        public static Participant CreateBoxer(int height, double weightInKg, string firstName, string lastName, string nickName)
        {
            Participant boxer = new Participant();

            int weight = Convert.ToInt32(weightInKg * 1000);
            
            boxer.WeightInGrams = weight;
            boxer.HeightInCm = height;
            boxer.FirstName = firstName;
            boxer.LastName = lastName;
            boxer.NickName = nickName;

            switch (weightInKg)
            {
                case var exp when (weightInKg <= 1):
                    boxer.Division = "An actual fly or something";
                    Console.WriteLine("Only humans allowed in this competition I am afraid.");
                    break;
                case var exp when (weightInKg <= 46.27):
                    boxer.Division = "Light minimumweight";
                    break;
                case var exp when (weightInKg <= 47.63):
                    boxer.Division = "Minimumweight";
                    break;
                case var exp when (weightInKg <= 48.99):
                    boxer.Division = "Light Flyweight";
                    break;
                case var exp when (weightInKg <= 50.8):
                    boxer.Division = "Flyweight";
                    break;
                case var exp when (weightInKg <= 52.16):
                    boxer.Division = "Super flyweight"; 
                    break;
                case var exp when (weightInKg <= 53.52):
                    boxer.Division = "Bantamweight"; 
                    break;
                case var exp when (weightInKg <= 55.34):
                    boxer.Division = "Super bantamweight"; 
                    break;
                case var exp when (weightInKg <= 57.15):
                    boxer.Division = "Featerweight"; 
                    break;
                case var exp when (weightInKg <= 58.97):
                    boxer.Division = "Super featherweight"; 
                    break;
                case var exp when (weightInKg <= 61.23):
                    boxer.Division = "Lightweight"; 
                    break;
                case var exp when (weightInKg <= 63.5):
                    boxer.Division = "Super lightweight"; 
                    break;
                case var exp when (weightInKg <= 66.68):
                    boxer.Division = "Welterweight"; 
                    break;
                case var exp when (weightInKg <= 69.85):
                    boxer.Division = "Super welterweight"; 
                    break;
                case var exp when (weightInKg <= 72.57):
                    boxer.Division = "Middleweight"; 
                    break;
                case var exp when (weightInKg <= 76.2):
                    boxer.Division = "Super middleweight"; 
                    break;
                case var exp when (weightInKg <= 79.38):
                    boxer.Division = "Light heavyweight"; 
                    break;
                case var exp when (weightInKg <= 90.72):
                    boxer.Division = "Cruiserweight"; 
                    break;
                case var exp when (weightInKg <= 350):
                    boxer.Division = "Heavyweight";
                    break;
                case var exp when (weightInKg > 350):
                    boxer.Division = "Elephant or something";
                    Console.WriteLine("Only humans allowed in this competition.");
                    break;
                default:
                    boxer.Division = "unknown";
                    break;
            }

            return boxer;
        }
        public static void ClearDatabase()
        {
            using (CompetitionDbContext boxerContext = new CompetitionDbContext())
            {
                var allBoxers = boxerContext.Participants;
                var allSkills = boxerContext.Skills;
                var allCoaches = boxerContext.Coaches;
                var allTeams = boxerContext.Teams;

                boxerContext.Participants.RemoveRange(allBoxers);
                boxerContext.Coaches.RemoveRange(allCoaches);
                boxerContext.Skills.RemoveRange(allSkills);
                boxerContext.Teams.RemoveRange(allTeams);

                boxerContext.SaveChanges();
            }
        }
    }
}
